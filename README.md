# rocket chat snap backup script

Rocket Chat comes with a method for creating backups when installing via Snap. It does not deal with the automatic deletion of old backups. This script can be triggered via a corn-job and holds only the configured number of backups.